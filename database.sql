CREATE DATABASE IF NOT EXISTS hostaway;
use hostaway;

CREATE TABLE phonebook (
id int NOT NULL AUTO_INCREMENT,
fname varchar(255) NOT NULL,
lname varchar(255),
phone_number varchar(255) NOT NULL,
country_code varchar(255),
timezone_name varchar(255),
insertedOn datetime DEFAULT NOW(),
updatedOn datetime DEFAULT NOW() ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (id)
);