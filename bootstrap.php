<?php
const APP_DIR = __DIR__;
require APP_DIR . '/helpers.php';
require APP_DIR . '/vendor/autoload.php';

error_reporting(E_ALL);
spl_autoload_register(function($class) {
    $base_dir = APP_DIR . '/src';

    $prefix = 'App';
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }

    $relative_class = substr($class, $len);

    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
    if (file_exists($file)) {
        require $file;
    }
});
