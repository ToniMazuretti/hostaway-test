<?php
namespace App;

require_once 'bootstrap.php';

$router = new Router();

//create
$router->post('/records',
    \App\Controller\Records::class . '@create'
);

//read all
$router->get('/records',
    \App\Controller\Records::class . '@readAll'
);

//read
$router->get('/records/*',
    \App\Controller\Records::class . '@read'
);

//update
$router->put('/records/*',
    \App\Controller\Records::class . '@update'
);

//delete
$router->delete('/records/*',
    \App\Controller\Records::class . '@delete'
);

$application = new Application($router);
$application->run();
