<?php

namespace App\Controller;

use ActiveRecord\DatabaseException;
use ActiveRecord\UndefinedPropertyException;
use ActiveRecord\RecordNotFound;
use App\Exception\ApiException;
use App\Helper\RequestHelper;
use App\JsonResponse;
use Phonebooks;
use Exception;

/**
 * Class Records
 * @package App\Controller
 */
class Records
{
    /**
     * @param array $params
     *
     * @return JsonResponse
     * @throws ApiException
     */
    public function create(array $params = []): JsonResponse
    {
        $response = new JsonResponse();

        if (!RequestHelper::isValidJsonHeaders()) {
            throw new ApiException('Bad request', 400);
        }

        $record = Phonebooks::create($params);

        if ($record->is_invalid()) {
            $message = trim(implode(', ', $record->errors->full_messages()),',');
            throw new ApiException($message, 400);
        }

        $response->setContent($record = Phonebooks::find([$record->id])->to_array());
        $response->setCode(201);

        return $response;
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     * @throws ApiException
     * @throws RecordNotFound
     */
    public function read(int $id): JsonResponse
    {
        $response = new JsonResponse();

        if (!RequestHelper::acceptsJson()) {
            throw new ApiException('Bad request', 400);
        }

        $record = Phonebooks::find([$id]);
        $response->setContent($record->to_array());

        return $response;
    }

    /**
     * @param array $params
     *
     * @return JsonResponse
     * @throws ApiException
     */
    public function readAll($params = []): JsonResponse
    {
        $response = new JsonResponse();

        if (!RequestHelper::acceptsJson()) {
            throw new ApiException('Bad request', 400);
        }

        $query = [];
        $query['limit'] = (!empty($params['limit']) && $params['limit'] > 100) ? 100 : $params['limit'];
        $query['offset'] = (empty($params['offset'])) ? 0 : $params['offset'];

        if (!empty($params['q']) && strlen(trim($params['q'])) >= 3) {
            $query['conditions'] = ['fname LIKE \'%' . $params['q'] . '%\' OR lname LIKE \'%' . $params['q'] . '%\''];
        }

        $records = Phonebooks::all($query);
        $result = [];

        foreach ($records as $record) {
            $result[] = $record->to_array();
        }

        $response->setContent($result);

        return $response;
    }

    /**
     * @param int $id
     * @param array $params
     *
     * @return JsonResponse
     * @throws ApiException
     * @throws RecordNotFound
     */
    public function update(int $id, array $params = []): JsonResponse
    {
        $response = new JsonResponse();

        if (!RequestHelper::isValidJsonHeaders()) {
            throw new ApiException('Bad request', 400);
        }

        if (empty($params)) {
            throw new ApiException('Update data can\'t be empty', 400);
        }

        $record = Phonebooks::find([$id]);

        if ($record->update_attributes($params) === false) {
            throw new ApiException(
                trim(implode(', ', $record->errors->full_messages()),','),
                400
            );
        }

        $response->setContent($record->to_array());

        return $response;
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     * @throws ApiException
     * @throws RecordNotFound
     */
    public function delete(int $id): JsonResponse
    {
        $response = new JsonResponse();

        if (!RequestHelper::acceptsJson()) {
            throw new ApiException('Bad request', 400);
        }

        $record = Phonebooks::find([$id]);

        if ($record->delete() === false) {
            throw new ApiException($record->errors->full_messages(), 500);
        }

        $response->setContent(['result'=>'success']);

        return $response;
    }
}