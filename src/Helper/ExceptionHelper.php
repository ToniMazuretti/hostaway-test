<?php

namespace App\Helper;

use App\Exception\ApiException;
use Exception;

/**
 * Class ExceptionHelper
 * @package App\Helper
 */
class ExceptionHelper
{
    public const EXCEPTION_CLASS_CODE = [
        'ActiveRecord\DatabaseException' => 500,
        'ActiveRecord\UndefinedPropertyException' => 400,
        'ActiveRecord\RecordNotFound' => 404,
    ];

    /**
     * @param Exception $exception
     * @param int $defaultCode
     *
     * @return int
     */
    public static function getErrorCode(Exception $exception, int $defaultCode = 500)
    {
        return self::EXCEPTION_CLASS_CODE[$exception::class] ?: $defaultCode;
    }

    /**
     * @param Exception $exception
     *
     * @return ApiException
     */
    public static function proccess(Exception $exception): ApiException
    {
        return new ApiException(
            $exception->getMessage(),
            $exception->getCode() ?: self::getErrorCode($exception)
        );
    }
}