<?php

namespace App\Helper;

/**
 * Class RequestHelper
 * @package App\Helper
 */
class RequestHelper
{
    /**
     * @return bool
     */
    public static function isValidJsonHeaders(): bool
    {
        return static::isJson() && static::acceptsJson();
    }

    /**
     * @return bool
     */
    public static function acceptsJson(): bool
    {
        return $_SERVER['HTTP_ACCEPT'] == 'application/json';
    }

    /**
     * @return bool
     */
    public static function isJson(): bool
    {
        return str_contains($_SERVER['CONTENT_TYPE'], 'json');
    }
}