<?php
namespace App;

use App\Exception\ApiException;
use Throwable;

/**
 * Class FileCacheStorage
 * @package App
 */
class FileCacheStorage implements CacheStorageInterface
{
    private const PATH = APP_DIR . '/storage/cache';
    private const VALID_TIME = 300;

    /**
     * FileCacheStorage constructor.
     */
    public function __construct()
    {
        if (!is_dir(self::PATH)) {
            mkdir(self::PATH, 0755, true);
        }
    }

    /**
     * @param string $fileName
     *
     * @return bool|string|array
     * @throws ApiException
     */
    public function get(string $fileName): bool|string|array
    {
        $fileName = $this->genCacheId($fileName);

        if ($this->isValid($fileName) == false) {
            return false;
        }

        try {
            $data = file_get_contents(self::PATH . '/' . $fileName);
            if ($data == false) {
                return false;
            }

            $data = json_decode($data, true, 512, JSON_THROW_ON_ERROR);
        } catch (Throwable $exception) {
            throw new ApiException($exception->getMessage(), 500);
        }

        return $data;
    }

    /**
     * @param string $fileName
     * @param string|array $data
     *
     * @return bool
     * @throws ApiException
     */
    public function put(string $fileName, string|array $data): bool
    {
        if (empty($data)) {
            return false;
        }

        $fileName = $this->genCacheId($fileName);

        try {
            $data = json_encode($data);
        } catch (Throwable $exception) {
            throw new ApiException($exception->getMessage(), 500);
        }

        $fp = fopen(self::PATH . '/' . $fileName, 'w+');
        $result = fwrite($fp, $data);
        fclose($fp);

        return $result > 0 && $result != false;
    }

    /**
     * @param string $fileName
     *
     * @return bool
     */
    public function isValid(string $fileName): bool
    {
        static $valid = false;
        $fileName = $this->genCacheId($fileName);

        if (!$valid) {
            if (file_exists(self::PATH . '/' . $fileName) == false) {
                return $valid;
            }

            $valid = (time() - filemtime(self::PATH . '/' . $fileName)) < self::VALID_TIME;
        }

        return $valid;
    }

    /**
     * @param string $fileName
     *
     * @return string
     */
    private function genCacheId(string $fileName): string
    {
        return md5($fileName).'.json';
    }

}