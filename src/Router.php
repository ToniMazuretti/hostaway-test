<?php

namespace App;

use \App\HttpException;
use \App\Exception\NotFoundException;

/**
 * Class Router
 * @package App
 */
class Router
{
    protected $paths = [];

    public function post($path, $callback)
    {
        $path = trim($path, '/');
        $path = "/" . $path;
        $route = new Route('POST', $path, $callback);
        $this->paths['POST'][$path] = $route;
    }

    public function get($path, $callback)
    {
        $path = trim($path, '/');
        $path = "/" . $path;
        $route = new Route('GET', $path, $callback);
        $this->paths['GET'][$path] = $route;
    }

    public function put($path, $callback)
    {
        $path = trim($path, '/');
        $path = "/" . $path;
        $route = new Route('PUT', $path, $callback);
        $this->paths['PUT'][$path] = $route;
    }

    public function delete($path, $callback)
    {
        $path = trim($path, '/');
        $path = "/" . $path;
        $route = new Route('DELETE', $path, $callback);
        $this->paths['DELETE'][$path] = $route;
    }

    /**
     * @return mixed
     */
    public function dispatch()
    {
        foreach ($this->paths[$_SERVER['REQUEST_METHOD']] as $route) {
            if ($route->match($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI'])) {
                return $route->run($_SERVER['REQUEST_URI']);
                break;
            }
        }
        throw new NotFoundException('Страница не найдена', 404);
    }
}
