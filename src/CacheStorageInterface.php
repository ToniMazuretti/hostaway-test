<?php

namespace App;

/**
 * Interface CacheStorageInterface
 * @package App
 */
interface CacheStorageInterface
{
    public function get(string $id);
    
    public function put(string $fileName, string|array $data);

    public function isValid(string $fileName);
}