<?php
namespace App;

/**
 * Class Cache
 * @package App
 */
class Cache
{
    protected CacheStorageInterface $storage;

    /**
     * Cache constructor.
     *
     * @param CacheStorageInterface $storage
     */
    public function __construct(CacheStorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param string $cacheId
     *
     * @return bool|string|array
     */
    public function get(string $cacheId): bool|string|array
    {
        return $this->storage->get($cacheId);
    }

    public function put(string $cacheID, string|array $data)
    {
        return $this->storage->put($cacheID, $data);
    }

    public function isValid(string $cacheID): bool
    {
        return $this->storage->isValid($cacheID);
    }

}