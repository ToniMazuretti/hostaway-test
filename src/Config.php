<?php

namespace App;

use Exception;

/**
 * Class Config
 * @package App
 */
class Config
{
    protected array $configs;
    private static ?Config $instance = null;

    /**
     * @return Config
     */
    public static function getInstance(): Config
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    private function __construct()
    {
        $this->configs['db'] = include_once(APP_DIR . '/configs/db.php');
        $this->configs['app'] = include_once(APP_DIR . '/configs/app.php');
    }

    public function get($config, $default = null)
    {
        return array_get($this->configs, $config, $default);
    }

    protected function __clone()
    {
    }

    public function __wakeup()
    {
        throw new Exception("Cannot unserialize a singleton.");
    }
}
