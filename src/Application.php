<?php

namespace App;

use ActiveRecord\Config as ORMConfig;
use Exception;
use App\Config as AppConfig;
use App\Helper\ExceptionHelper;

/**
 * Class Application
 * @package App
 */
class Application
{
    protected Router $router;

    public function __construct(Router $router)
    {
        $this->initialize();
        $this->router = $router;
    }

    public function run()
    {
        try {
            $res = $this->router->dispatch();

            if ($res instanceof Renderable) {
                $res->render();
            } else {
                echo $res;
            }
        } catch (Exception $exception) {
            $this->renderException(ExceptionHelper::proccess($exception));
        }
    }

    /**
     * @param Exception $exception
     */
    protected function renderException(Exception $exception)
    {
        if ($exception instanceof Renderable) {
            $exception->render();
        } else {
            echo $exception->getMessage();
        }
    }

    protected function initialize()
    {
        ORMConfig::initialize(function($cfg) {
            $cfg->set_model_directory(APP_DIR . '/src/Model');
            $cfg->set_connections([
                'development' => 'mysql://' . AppConfig::getInstance()->get('db.user_name') .
                ':' . AppConfig::getInstance()->get('db.password', '') .
                '@' . Config::getInstance()->get('db.host') .
                '/' . AppConfig::getInstance()->get('db.db_name')
            ]);
        });
    }
}
