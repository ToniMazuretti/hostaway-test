<?php

namespace App;

/**
 * Class JsonResponse
 * @package App
 */
class JsonResponse extends Response
{

    public function render()
    {
        $this->setHeader(['Content-type', 'application/json']);
        $this->setContent(json_encode($this->getContent()));
        $this->sendHeaders();
        echo $this->getContent();
    }
}