<?php

namespace App;

/**
 * Class Response
 * @package App
 */
class Response implements Renderable
{
    private $content;
    private int $code;
    private array $header;
    private string $codeText;
    private string $version;

    /**
     * Response constructor.
     *
     * @param string $content
     * @param int $status
     * @param array $headers
     * @param string $codeText
     */
    public function __construct($content = '', $status = 200, $headers = [], $codeText = 'OK')
    {
        $this->setContent($content);
        $this->setCode($status);
        $this->setVersion('1.0');
        $this->setCodeText($codeText);
        $this->setHeader($headers);
    }

    public function render()
    {
        $this->sendHeaders();
        echo $this->getContent();
    }

    /**
     * @param array|string $content
     *
     * @return $this
     */
    public function setContent(array|string $content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @param string $value
     */
    public function setVersion(string $value)
    {
        $this->version = $value;
    }

    /**
     * @param string $codeText
     */
    public function setCodeText(string $codeText)
    {
        $this->codeText = $codeText;
    }

    /**
     * @param int $code
     *
     * @return $this
     */
    public function setCode(int $code)
    {
        if (!$this->validateCode($code)) {
            //TODO:: throw exception
        }

        $this->code = $code;

        return $this;
    }

    /**
     * @param int $code
     *
     * @return bool
     */
    protected function validateCode(int $code = 200)
    {
        return true;
    }

    protected function sendHeaders()
    {
        if (headers_sent()) {
            return;
        }

        foreach ($this->header as $item) {
            if (empty($item[0]) || empty($item[1])) {
                continue;
            }

            header($item[0] . ': ' . $item[1], false, $this->code);
        }

        header(sprintf('HTTP/%s %s %s', $this->version, $this->code, $this->codeText), true, $this->code);
    }

    /**
     * @param array $value
     */
    public function setHeader(array $value): void
    {
        if (empty($value)) {
            return;
        }

        $this->header[] = $value;
    }

    public function getContent()
    {
        return $this->content;
    }

}
