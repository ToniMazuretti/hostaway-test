<?php

namespace App;

use Exception;

/**
 * Class Route
 * @package App
 */
class Route
{
    private string $method;
    private string $path;
    private array $callback;

    /**
     * Route constructor.
     *
     * @param string $method
     * @param string $path
     * @param string $callback
     *
     * @throws Exception
     */
    public function __construct(string $method, string $path, string $callback)
    {
        $this->method = $method;
        $this->path = $path;
        $this->callback = $this->prepareCallback($callback);
    }

    /**
     * @param string $callback
     *
     * @return array
     */
    protected function getCallbackFromString(string $callback): array
    {
        return explode('@', $callback);
    }

    /**
     * @param string $callback
     *
     * @return array
     * @throws Exception
     */
    private function prepareCallback(string $callback): array
    {
        $callback = $this->getCallbackFromString($callback);
        if (method_exists($callback[0], $callback[1])) {

            return $callback;
        }
        
        throw new Exception('Callback ' . (string)$callback . ' не может быть вызван');
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $method
     * @param string $uri
     *
     * @return false|int
     */
    public function match(string $method, string $uri)
    {
        if ($this->method != $method) {
            return false;
        }
        $uri = trim(parse_url($uri, PHP_URL_PATH));
        $uri = trim($uri, '/');
        $uri = '/' . $uri;

        return preg_match('/^' . str_replace(['*', '/'], ['\w+', '\/'], $this->getPath()) . '$/', $uri);
    }

    /**
     * @param string $uri
     *
     * @return false|mixed
     */
    public function run(string $uri)
    {
        $fakeUri = 'http://www.fakedomain.com' . $uri;
        $query = '';
        $path = $uri;
        $params = [];

        if (filter_var($fakeUri, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED)) {
            $query = parse_url($fakeUri, PHP_URL_QUERY);
            $path = parse_url($fakeUri, PHP_URL_PATH);
        }

        $uriItems = explode('/', trim($path, '/'));
        $pathItems = explode('/', trim($this->path, '/'));

        if (count($uriItems) && count($pathItems)) {
            foreach ($pathItems as $index => $item) {
                if ($item == '*') {
                    $params[$index] = $uriItems[$index];
                }
            }
        }

        switch ($this->method) {
            case 'GET':
                if (!empty($query)) {
                    parse_str($query, $queryParams);
                    $params[] = $queryParams;
                }
                break;
            case 'POST':
                $params = [json_decode(file_get_contents("php://input"), true)];
                break;
            case 'PUT':
                $params[] = json_decode(file_get_contents("php://input"), true);
                break;
            case 'DELETE':
                break;
            default:
                throw new HttpException('Route method ' . $this->method . ' is not defined');
        }

        // The place for dipendency incjection container $container->call($controller, $parameters);
        return call_user_func_array([new $this->callback[0], $this->callback[1]], $params);
    }
}
