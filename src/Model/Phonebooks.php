<?php

use ActiveRecord\Model;
use App\Config as AppConfig;
use App\Exception\ApiException;
use GuzzleHttp\Client;
use App\Cache;
use App\FileCacheStorage;

/**
 * Class Phonebooks
 */
class Phonebooks extends Model
{
    static $table_name = 'phonebook';
    static $validates_presence_of = [['fname'], ['phone_number']];
    private const COUNTRY_CODES_URL = 'https://api.hostaway.com/countries';
    private const PHONE_CODES_URL = 'https://api.hostaway.com/timezones';

    public function validate()
    {
        if (!empty($this->country_code) && $this->validateCountryCode($this->country_code) == false) {
            $this->errors->add('country_code', ': this value is not allowed');
        }

        if (!empty($this->timezone_name) && $this->validateTimezoneCode($this->timezone_name) == false) {
            $this->errors->add('timezone_name', ': this value is not allowed');
        }

        if (!empty($this->phone_number) && $this->validatePhoneNumber($this->phone_number) == false) {
            $this->errors->add('phone_number', ': format is wrong');
        }
    }

    /**
     * @param string $phoneNumber
     *
     * @return bool
     */
    private function validatePhoneNumber(string $phoneNumber): bool
    {
        if (preg_match("/^[+0-9]{1,3}\s[0-9]{3}\s[0-9]{7,9}$/", $phoneNumber)) {
           return true;
        }

        return false;
    }

    /**
     * @param string $countryCode
     *
     * @return bool
     */
    private function validateCountryCode(string $countryCode): bool
    {
        $cache = new Cache(new FileCacheStorage());

        if ($cache->isValid(self::COUNTRY_CODES_URL)) {
            $validList = $cache->get(self::COUNTRY_CODES_URL);
        } else {
            $validList = $this->getValidationData(self::COUNTRY_CODES_URL);
            try {
                $cache->put(
                    self::COUNTRY_CODES_URL,
                    $validList
                );
            } catch (Throwable $exception) {
                //TODO:: report cache write problem
            }
        }

        return !empty($validList[$countryCode]);
    }

    /**
     * @param string $timezoneCode
     *
     * @return bool
     */
    private function validateTimezoneCode(string $timezoneCode): bool
    {
        $cache = new Cache(new FileCacheStorage());

        if ($cache->isValid(self::PHONE_CODES_URL)) {
            $validList = $cache->get(self::PHONE_CODES_URL);
        } else {
            $validList = $this->getValidationData(self::PHONE_CODES_URL);
            $cache->put(
                self::PHONE_CODES_URL,
                $validList
            );
        }

        return !empty($validList[$timezoneCode]);
    }

    /**
     * @param string $url
     *
     * @return array
     */
    private function getValidationData(string $url): array
    {
        try {
            $res = (new Client())->request(
                'GET',
                $url,
                ['connect_timeout' => AppConfig::getInstance()->get('app.http_request_timeout')]
            );

            if ($res->getStatusCode() != 200) {
                return [];
            }

            $validList = $res->getBody()->getContents();

            if (empty($validList)) {
                return [];
            }

            try {
                $validList = json_decode($validList, true, 512, JSON_THROW_ON_ERROR);
            } catch (\Throwable $exception) {
                throw new ApiException($exception->getMessage(), 500);
            }

            if (empty($validList['result']) || $validList['status'] != 'success') {
                return [];
            }
        } catch (Throwable $exception) {
            throw new ApiException($exception->getMessage(), 500);

        }

        return $validList['result'];
    }

}