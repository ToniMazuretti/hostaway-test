<?php

namespace App\Exception;

use App\Renderable;
use Exception;

class ApiException extends Exception implements Renderable
{
    public function render()
    {
        header('Content-type: application/json', false, $this->code);
        echo json_encode([$this->getMessage()], true);
    }
}
