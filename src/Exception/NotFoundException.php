<?php

namespace App\Exception;

use App\Renderable;

/**
 * Class NotFoundException
 * @package App\Exception
 */
class NotFoundException extends ApiException implements Renderable
{
    
}
